/*******************************************************************************
 * am_video_service_msg_action.cpp
 *
 * History:
 *   Sep 18, 2015 - [ypchang] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#include "am_base_include.h"
#include "am_define.h"
#include "am_log.h"

#include "am_api_video.h"
#include "am_video_types.h"
#include "am_video_utility.h"
#include "am_service_frame_if.h"
#include "am_video_camera_if.h"
/* Videp Plugin Interface */
#include "am_encode_warp_if.h"
#include "am_low_bitrate_control_if.h"
#include "am_encode_eis_if.h"
#include "am_encode_overlay_if.h"
#include "am_dptz_if.h"
#include <signal.h>

extern AMIVideoCameraPtr g_video_camera;
extern AMIServiceFrame  *g_service_frame;
extern AM_SERVICE_STATE  g_service_state;

void ON_SERVICE_INIT(void *msg_data,
                     int msg_data_size,
                     void *result_addr,
                     int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  svc_ret->ret = 0;
  svc_ret->state = g_service_state;
  if (AM_LIKELY(svc_ret->state == AM_SERVICE_STATE_INIT_DONE)) {
    INFO("Video Service init done!");
  }
}

void ON_SERVICE_DESTROY(void *msg_data,
                        int msg_data_size,
                        void *result_addr,
                        int result_max_size)
{
  INFO("Video Service destroying");
  g_video_camera->stop();
  g_service_frame->quit();
  INFO("Video Service destroyed!");
}

void ON_SERVICE_START(void *msg_data,
                      int msg_data_size,
                      void *result_addr,
                      int result_max_size)
{
  int32_t ret = 0;
  if (AM_LIKELY(g_video_camera)) {
    if (AM_UNLIKELY(g_service_state != AM_SERVICE_STATE_STARTED)) {
      if (AM_UNLIKELY(AM_RESULT_OK != g_video_camera->start())) {
        ERROR("Video Service: Failed to start!");
        g_service_state = AM_SERVICE_STATE_ERROR;
        ret = -2;
      } else {
        g_service_state = AM_SERVICE_STATE_STARTED;
      }
    }
  } else {
    ERROR("Video Service: Failed to get AMVideoCamera instance!");
    ret = -1;
    g_service_state = AM_SERVICE_STATE_NOT_INIT;
  }
  ((am_service_result_t*)result_addr)->ret = ret;
  ((am_service_result_t*)result_addr)->state = g_service_state;
}

void ON_SERVICE_STOP(void *msg_data,
                     int msg_data_size,
                     void *result_addr,
                     int result_max_size)
{
  int32_t ret = 0;
  if (AM_LIKELY(g_video_camera)) {
    if (AM_UNLIKELY(AM_RESULT_OK != g_video_camera->stop())) {
      ERROR("Video Service: Failed to stop!");
      ret = -2;
      g_service_state = AM_SERVICE_STATE_ERROR;
    } else {
      g_service_state = AM_SERVICE_STATE_STOPPED;
    }
  } else {
    ERROR("Video Service: Failed to get AMVideoCamera instance!");
    ret = -1;
    g_service_state = AM_SERVICE_STATE_NOT_INIT;
  }
  ((am_service_result_t*)result_addr)->ret = ret;
  ((am_service_result_t*)result_addr)->state = g_service_state;
}

void ON_SERVICE_RESTART(void *msg_data,
                        int msg_data_size,
                        void *result_addr,
                        int result_max_size)
{
  NOTICE("Not implemented!");
}

void ON_SERVICE_STATUS(void *msg_data,
                       int msg_data_size,
                       void *result_addr,
                       int result_max_size)
{
  ((am_service_result_t*)result_addr)->ret = 0;
  ((am_service_result_t*)result_addr)->state = g_service_state;
  INFO("Video Service: Get status!");
}


void ON_CFG_ALL_LOAD(void *msg_data,
                     int msg_data_size,
                     void *result_addr,
                     int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("Video Service: ON_CFG_ALL_LOAD");

  if (AM_RESULT_OK != g_video_camera->load_config_all()) {
    svc_ret->ret = -1;
    ERROR("Failed to load all configuration!");
  }
}

void ON_CFG_FEATURE_GET(void *msg_data,
                        int msg_data_size,
                        void *result_addr,
                        int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("Video Service: ON_CFG_FEATURE_GET");
  do {
    am_feature_config_t *feature = (am_feature_config_t *)svc_ret->data;
    AMFeatureParam param;
    if (AM_RESULT_OK != g_video_camera->get_feature_config(param)) {
      svc_ret->ret = -1;
      ERROR("Failed to get feature config!");
      break;
    }
    feature->version = param.version.second;
    feature->mode = param.mode.second;
    feature->hdr = param.hdr.second;
    feature->iso = param.iso.second;
    feature->dewarp_func = param.dewarp_func.second;
    feature->dptz = param.dptz.second;
    feature->bitrate_ctrl = param.bitrate_ctrl.second;
    feature->overlay = param.overlay.second;
  } while (0);
}

void ON_CFG_FEATURE_SET(void *msg_data,
                        int msg_data_size,
                        void *result_addr,
                        int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("Video Service: ON_CFG_FEATURE_SET");

  do {
    am_feature_config_t *feature = (am_feature_config_t *)msg_data;
    AMFeatureParam param;
    if (TEST_BIT(feature->enable_bits, AM_FEATURE_CONFIG_MODE_EN_BIT)) {
      param.mode.first = true;
      param.mode.second = AM_ENCODE_MODE(feature->mode);
    }
    if (TEST_BIT(feature->enable_bits, AM_FEATURE_CONFIG_HDR_EN_BIT)) {
      param.hdr.first = true;
      param.hdr.second = AM_HDR_TYPE(feature->hdr);
    }
    if (TEST_BIT(feature->enable_bits, AM_FEATURE_CONFIG_ISO_EN_BIT)) {
      param.iso.first = true;
      param.iso.second = AM_IMAGE_ISO_TYPE(feature->iso);
    }
    if (TEST_BIT(feature->enable_bits, AM_FEATURE_CONFIG_DEWARP_EN_BIT)) {
      param.iso.first = true;
      param.dewarp_func.second = AM_DEWARP_FUNC_TYPE(feature->dewarp_func);
    }
    if (TEST_BIT(feature->enable_bits, AM_FEATURE_CONFIG_BITRATECTRL_EN_BIT)) {
      param.bitrate_ctrl.first = true;
      param.bitrate_ctrl.second = AM_BITRATE_CTRL_METHOD(feature->bitrate_ctrl);
    }
    if (TEST_BIT(feature->enable_bits, AM_FEATURE_CONFIG_OVERLAY_EN_BIT)) {
      param.overlay.first = true;
      param.overlay.second = AM_OVERLAY_TYPE(feature->overlay);
    }
    if (AM_RESULT_OK != g_video_camera->set_feature_config(param)) {
      svc_ret->ret = -1;
      ERROR("Failed to get feature config!");
      break;
    }

  } while (0);
}


void ON_CFG_VIN_GET(void *msg_data,
                    int msg_data_size,
                    void *result_addr,
                    int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("Video Service: ON_CFG_VIN_GET");

  if (!msg_data) {
    svc_ret->ret = -1;
    ERROR("msg_data is NULL");
  } else {
    do {
      am_vin_config_t *air_config = (am_vin_config_t*)svc_ret->data;
      uint32_t vin_id = air_config->vin_id;
      AMVinParamMap vin_param_map;
      if (AM_RESULT_OK != g_video_camera->get_vin_config(vin_param_map)) {
        svc_ret->ret = -1;
        ERROR("Failed to get vin configuration!");
        break;
      }
      AMVinParam vin_param = vin_param_map[AM_VIN_ID(vin_id)];
      air_config->width = AMVinTrans::mode_to_resolution(vin_param.mode.second).width;
      air_config->height = AMVinTrans::mode_to_resolution(vin_param.mode.second).height;
      switch (vin_param.flip.second) {
        case AM_VIDEO_FLIP_VERTICAL:
          air_config->flip = 1;
          break;
        case AM_VIDEO_FLIP_HORIZONTAL:
          air_config->flip = 2;
          break;
        case AM_VIDEO_FLIP_VH_BOTH:
          air_config->flip = 3;
          break;
        case AM_VIDEO_FLIP_AUTO:
          air_config->flip = 255;
          break;
        case AM_VIDEO_FLIP_NONE:
        default:
          air_config->flip = 0;
          break;
      }
      air_config->fps = vin_param.flip.second;
      switch (vin_param.bayer_pattern.second) {
        case AM_VIN_BAYER_PATTERN_RG:
          air_config->bayer_pattern = 1;
          break;
        case AM_VIN_BAYER_PATTERN_BG:
          air_config->bayer_pattern = 2;
          break;
        case AM_VIN_BAYER_PATTERN_GR:
          air_config->bayer_pattern = 3;
          break;
        case AM_VIN_BAYER_PATTERN_GB:
          air_config->bayer_pattern = 4;
          break;
        case AM_VIN_BAYER_PATTERN_AUTO:
        default:
          air_config->bayer_pattern = 0;
          break;
      }
    } while (0);
  }
}

void ON_CFG_VIN_SET(void *msg_data,
                    int msg_data_size,
                    void *result_addr,
                    int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: VIN set");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_vin_config_t *vin_conf = (am_vin_config_t*)msg_data;
    AMVinParamMap    vin_param_map;
    AM_RESULT result = g_video_camera->get_vin_config(vin_param_map);
    if (AM_UNLIKELY(AM_RESULT_OK != result)) {
      ERROR("Failed to get VIN configuration!");
      svc_ret->ret = -2;
    } else {
      AMVinParam &vin_param = vin_param_map[AM_VIN_ID(vin_conf->vin_id)];
      if (TEST_BIT(vin_conf->enable_bits, AM_VIN_CONFIG_WIDTH_HEIGHT_EN_BIT)) {

      }
      if (TEST_BIT(vin_conf->enable_bits, AM_VIN_CONFIG_FLIP_EN_BIT)) {
        vin_param.flip.first = true;
        vin_param.flip.second = AM_VIDEO_FLIP(vin_conf->flip);
      }
      if (TEST_BIT(vin_conf->enable_bits, AM_VIN_CONFIG_FPS_EN_BIT)) {
        vin_param.fps.first = true;
        vin_param.fps.second = AM_VIDEO_FPS(vin_conf->fps);
      }
      if (TEST_BIT(vin_conf->enable_bits, AM_VIN_CONFIG_BAYER_PATTERN_EN_BIT)) {
        vin_param.bayer_pattern.first = true;
        vin_param.bayer_pattern.second =
            AM_VIN_BAYER_PATTERN(vin_conf->bayer_pattern);
      }
      result = g_video_camera->set_vin_config(vin_param_map);
      svc_ret->ret = (AM_RESULT_OK == result) ? 0 : -3;
    }
  }
}

void ON_CFG_VOUT_GET(void *msg_data,
                    int msg_data_size,
                    void *result_addr,
                    int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("Video Service: ON_CFG_VOUT_GET");

  if (!msg_data) {
    svc_ret->ret = -1;
    ERROR("msg_data is NULL");
  } else {
    do {
      am_vout_config_t *config = (am_vout_config_t*)svc_ret->data;
      AMVoutParamMap vout_param_map;
      if (AM_RESULT_OK != g_video_camera->get_vout_config(vout_param_map)) {
        svc_ret->ret = -3;
        ERROR("Failed to get vout configuration!");
        break;
      }

      AM_VOUT_ID *id = (AM_VOUT_ID *)msg_data;
      AMVoutParamMap::iterator it = vout_param_map.find(*id);
      if (it == vout_param_map.end()) {
        svc_ret->ret = -1;
        ERROR("vout%d config is null", *id);
        break;
      }
      AMVoutParam param = vout_param_map[*id];

      config->type = uint32_t(param.type.second);
      config->video_type = uint32_t(param.video_type.second);
      switch (param.flip.second) {
        case AM_VIDEO_FLIP_VERTICAL:
          config->flip = 1;
          break;
        case AM_VIDEO_FLIP_HORIZONTAL:
          config->flip = 2;
          break;
        case AM_VIDEO_FLIP_VH_BOTH:
          config->flip = 3;
          break;
        case AM_VIDEO_FLIP_AUTO:
          config->flip = 0;
          break;
        case AM_VIDEO_FLIP_NONE:
        default:
          config->flip = 0;
          break;
      }
      config->rotate = uint32_t(param.rotate.second);
      config->fps = uint32_t(param.fps.second);
      std::string mode = AMVoutTrans::mode_enum_to_str(param.mode.second);
      strncpy(config->mode, mode.c_str(), mode.size());
    } while (0);
  }
}

void ON_CFG_VOUT_SET(void *msg_data,
                     int msg_data_size,
                     void *result_addr,
                     int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: VOUT set");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_vout_config_t *config = (am_vout_config_t*)msg_data;
    AMVoutParamMap    param_map;
    AMVoutParam &param = param_map[AM_VOUT_ID(config->vout_id)];
    if (TEST_BIT(config->enable_bits, AM_VOUT_CONFIG_TYPE_EN_BIT)) {
      param.type.first = true;
      param.type.second = AM_VOUT_TYPE(config->type);
    }
    if (TEST_BIT(config->enable_bits, AM_VOUT_CONFIG_VIDEO_TYPE_EN_BIT)) {
      param.video_type.first = true;
      param.video_type.second = AM_VOUT_VIDEO_TYPE(config->video_type);
    }
    if (TEST_BIT(config->enable_bits, AM_VOUT_CONFIG_MODE_EN_BIT)) {
      param.mode.first = true;
      param.mode.second = AMVoutTrans::mode_str_to_enum(config->mode);
    }
    if (TEST_BIT(config->enable_bits, AM_VOUT_CONFIG_FLIP_EN_BIT)) {
      param.flip.first = true;
      param.flip.second = AM_VIDEO_FLIP(config->flip);
    }
    if (TEST_BIT(config->enable_bits, AM_VOUT_CONFIG_ROTATE_EN_BIT)) {
      param.rotate.first = true;
      param.rotate.second = AM_VIDEO_ROTATE(config->rotate);
    }
    if (TEST_BIT(config->enable_bits, AM_VOUT_CONFIG_FPS_EN_BIT)) {
      param.fps.first = true;
      param.fps.second = AM_VIDEO_FPS(config->fps);
    }
    if (g_video_camera->set_vout_config(param_map) != AM_RESULT_OK) {
      svc_ret->ret = -3;
    }
  }
}

void ON_CFG_BUFFER_GET(void *msg_data,
                       int msg_data_size,
                       void *result_addr,
                       int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Buffer Format Get");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    AMBufferParamMap buffer_param_map;
    am_buffer_fmt_t *buffer_fmt = (am_buffer_fmt_t*)svc_ret->data;
    AM_SOURCE_BUFFER_ID id = AM_SOURCE_BUFFER_ID((*(uint32_t*)msg_data));

    do {
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->get_buffer_config(buffer_param_map))) {
        ERROR("Failed to get buffer config!");
        svc_ret->ret = -2;
        break;
      }
      AMBufferConfigParam &buf_conf = buffer_param_map[id];
      buffer_fmt->buffer_id      = id;
      buffer_fmt->type           = buf_conf.type.second;
      buffer_fmt->input_crop     = buf_conf.input_crop.second ? 1 : 0;
      buffer_fmt->input_width    = buf_conf.input.second.size.width;
      buffer_fmt->input_height   = buf_conf.input.second.size.height;
      buffer_fmt->input_offset_x = buf_conf.input.second.offset.x;
      buffer_fmt->input_offset_y = buf_conf.input.second.offset.y;
      buffer_fmt->width          = buf_conf.size.second.width;
      buffer_fmt->height         = buf_conf.size.second.height;
      buffer_fmt->prewarp        = buf_conf.prewarp.second;
    } while(0);
  }
}

void ON_CFG_BUFFER_SET(void *msg_data,
                       int msg_data_size,
                       void *result_addr,
                       int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Buffer Format Set");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_buffer_fmt_t *buffer_fmt = ((am_buffer_fmt_t*)msg_data);
    AM_SOURCE_BUFFER_ID id = AM_SOURCE_BUFFER_ID(buffer_fmt->buffer_id);
    AMBufferParamMap buffer_param_map;
    AMBufferConfigParam buf_conf;

    do {
      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_TYPE_EN_BIT)) {
        buf_conf.type.first = true;
        buf_conf.type.second = AM_SOURCE_BUFFER_TYPE(buffer_fmt->type);
      }

      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_INPUT_CROP_EN_BIT)) {
        buf_conf.input_crop.first = true;
        buf_conf.input_crop.second = buffer_fmt->input_crop;
      }

      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_INPUT_WIDTH_EN_BIT)) {
        buf_conf.input.first = true;
        buf_conf.input.second.size.width = buffer_fmt->input_width;
      }

      if (TEST_BIT(buffer_fmt->enable_bits,
                   AM_BUFFER_FMT_INPUT_HEIGHT_EN_BIT)) {
        buf_conf.input.first = true;
        buf_conf.input.second.size.height = buffer_fmt->input_height;
      }

      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_INPUT_X_EN_BIT)) {
        buf_conf.input.first = true;
        buf_conf.input.second.offset.x = buffer_fmt->input_offset_x;
      }

      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_INPUT_Y_EN_BIT)) {
        buf_conf.input.first = true;
        buf_conf.input.second.offset.y = buffer_fmt->input_offset_y;
      }

      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_WIDTH_EN_BIT)) {
        buf_conf.size.first = true;
        buf_conf.size.second.width = buffer_fmt->width;
      }

      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_HEIGHT_EN_BIT)) {
        buf_conf.size.first = true;
        buf_conf.size.second.height = buffer_fmt->height;
      }

      if (TEST_BIT(buffer_fmt->enable_bits, AM_BUFFER_FMT_PREWARP_EN_BIT)) {
        buf_conf.prewarp.first = true;
        buf_conf.prewarp.second = buffer_fmt->prewarp;
      }

      buffer_param_map[id] = buf_conf;
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->set_buffer_config(buffer_param_map))) {
        ERROR("Failed to set buffer config!");
        svc_ret->ret = -2;
        break;
      }
    } while(0);
  }
}

void ON_CFG_STREAM_FMT_GET(void *msg_data,
                           int msg_data_size,
                           void *result_addr,
                           int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Stream Format Get");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    uint32_t stream_id = *((uint32_t*)msg_data);
    am_stream_fmt_t *fmt = (am_stream_fmt_t*)svc_ret->data;
    do {
      AMStreamParamMap stream_param_map;
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->get_stream_config(stream_param_map))) {
        ERROR("Failed to get stream configuration!");
        svc_ret->ret = -2;
        break;
      }
      AMStreamFormatConfig &stream_fmt =
          stream_param_map[AM_STREAM_ID(stream_id)].stream_format.second;
      fmt->enable = stream_fmt.enable.second;
      switch(stream_fmt.type.second) {
        case AM_STREAM_TYPE_NONE: {
          fmt->type = 0;
        }break;
        case AM_STREAM_TYPE_H264: {
          fmt->type = 1;
        }break;
        case AM_STREAM_TYPE_H265: {
          fmt->type = 2;
        }break;
        case AM_STREAM_TYPE_MJPEG: {
          fmt->type = 3;
        }break;
        default: {
          fmt->type = 0;
          ERROR("Wrong video type: %d, reset to none!", stream_fmt.type.second);
        }break;
      }
      fmt->source         = stream_fmt.source.second;
      fmt->frame_fact_num = stream_fmt.fps.second.mul;
      fmt->frame_fact_den = stream_fmt.fps.second.div;
      fmt->width          = stream_fmt.enc_win.second.size.width;
      fmt->height         = stream_fmt.enc_win.second.size.height;
      fmt->offset_x       = stream_fmt.enc_win.second.offset.x;
      fmt->offset_y       = stream_fmt.enc_win.second.offset.y;
      switch(stream_fmt.flip.second) {
        case AM_VIDEO_FLIP_VERTICAL: {
          fmt->hflip = 0;
          fmt->vflip = 1;
        } break;
        case AM_VIDEO_FLIP_HORIZONTAL: {
          fmt->hflip = 1;
          fmt->vflip = 0;
        } break;
        case AM_VIDEO_FLIP_VH_BOTH: {
          fmt->hflip = 1;
          fmt->vflip = 1;
        } break;
        case AM_VIDEO_FLIP_NONE:
        case AM_VIDEO_FLIP_AUTO:
        default: {
          fmt->hflip = 0;
          fmt->vflip = 0;
        } break;
      }
      fmt->rotate = stream_fmt.rotate_90_ccw.second ? 1 : 0;
    } while(0);
  }
}

void ON_CFG_STREAM_FMT_SET(void *msg_data,
                           int msg_data_size,
                           void *result_addr,
                           int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Stream Format Set");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_stream_fmt_t *fmt = (am_stream_fmt_t*)msg_data;
    do {
      AMStreamParamMap stream_param_map;
      AMStreamConfigParam stream_cfg;
      AMStreamFormatConfig &stream_fmt = stream_cfg.stream_format.second;

      stream_cfg.stream_format.first = true;

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_ENABLE_EN_BIT)) {
        stream_fmt.enable.first = true;
        stream_fmt.enable.second = fmt->enable;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_TYPE_EN_BIT)) {
        stream_fmt.type.first = true;
        stream_fmt.type.second = AM_STREAM_TYPE(fmt->type);
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_SOURCE_EN_BIT)) {
        stream_fmt.source.first = true;
        stream_fmt.source.second = AM_SOURCE_BUFFER_ID(fmt->source);
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_FRAME_NUM_EN_BIT)) {
        stream_fmt.fps.first = true;
        stream_fmt.fps.second.mul = fmt->frame_fact_num;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_FRAME_DEN_EN_BIT)) {
        stream_fmt.fps.first = true;
        stream_fmt.fps.second.div = fmt->frame_fact_den;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_WIDTH_EN_BIT)) {
        stream_fmt.enc_win.first = true;
        stream_fmt.enc_win.second.size.width = fmt->width;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_HEIGHT_EN_BIT)) {
        stream_fmt.enc_win.first = true;
        stream_fmt.enc_win.second.size.height = fmt->height;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_OFFSET_X_EN_BIT)) {
        stream_fmt.enc_win.first = true;
        stream_fmt.enc_win.second.offset.x = fmt->offset_x;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_OFFSET_Y_EN_BIT)) {
        stream_fmt.enc_win.first = true;
        stream_fmt.enc_win.second.offset.y = fmt->offset_y;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_ROTATE_EN_BIT)) {
        stream_fmt.rotate_90_ccw.first = true;
        stream_fmt.rotate_90_ccw.second = fmt->rotate;
      }

      if (TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_HFLIP_EN_BIT) ||
          TEST_BIT(fmt->enable_bits, AM_STREAM_FMT_VFLIP_EN_BIT)) {
        stream_fmt.flip.first = true;
        if (fmt->hflip && fmt->vflip) {
          stream_fmt.flip.second = AM_VIDEO_FLIP_VH_BOTH;
        } else if (fmt->hflip) {
          stream_fmt.flip.second = AM_VIDEO_FLIP_HORIZONTAL;
        } else if (fmt->vflip) {
          stream_fmt.flip.second = AM_VIDEO_FLIP_VERTICAL;
        } else {
          /* Should never come here */
          stream_fmt.flip.second = AM_VIDEO_FLIP_AUTO;
        }
      }

      stream_param_map[AM_STREAM_ID(fmt->stream_id)] = stream_cfg;
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->set_stream_config(stream_param_map))) {
        ERROR("Failed to set stream format!");
        svc_ret->ret = -2;
        break;
      }
    } while(0);
  }
}

void ON_CFG_STREAM_H264_GET(void *msg_data,
                            int msg_data_size,
                            void *result_addr,
                            int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("Video Service: Stream Config Get");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_h264_cfg_t *air_cfg = (am_h264_cfg_t*)svc_ret->data;
    do {
      AMStreamParamMap stream_param_map;
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->get_stream_config(stream_param_map))) {
        ERROR("Failed to get stream configuration!");
        svc_ret->ret = -2;
        break;
      }
      AMStreamH264Config &h264_cfg =
          stream_param_map[AM_STREAM_ID(air_cfg->stream_id)].h264_config.second;

      /* H.264 Config */
      air_cfg->bitrate_ctrl = h264_cfg.bitrate_control.second;
      air_cfg->profile = h264_cfg.profile_level.second;
      air_cfg->au_type = h264_cfg.au_type.second;
      air_cfg->M = h264_cfg.M.second;
      air_cfg->N = h264_cfg.N.second;
      air_cfg->idr_interval = h264_cfg.idr_interval.second;
      air_cfg->target_bitrate = h264_cfg.target_bitrate.second;
      air_cfg->mv_threshold = h264_cfg.mv_threshold.second;
      air_cfg->flat_area_improve = h264_cfg.flat_area_improve.second;
      air_cfg->multi_ref_p = h264_cfg.multi_ref_p.second;
      air_cfg->fast_seek_intvl = h264_cfg.fast_seek_intvl.second;
    }while(0);
  }
}

void ON_CFG_STREAM_H264_SET(void *msg_data,
                            int msg_data_size,
                            void *result_addr,
                            int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Stream Config Set");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_h264_cfg_t *air_cfg = (am_h264_cfg_t*)msg_data;
    do {
      AMStreamParamMap stream_param_map;
      AMStreamConfigParam stream_cfg;
      AMStreamH264Config &h264_cfg = stream_cfg.h264_config.second;
      stream_cfg.h264_config.first = true;

      /* H.264 Config */
      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_BITRATE_CTRL_EN_BIT)) {
        h264_cfg.bitrate_control.first = true;
        h264_cfg.bitrate_control.second =
            AM_H26X_RATE_CONTROL(air_cfg->bitrate_ctrl);
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_PROFILE_EN_BIT)) {
        h264_cfg.profile_level.first = true;
        h264_cfg.profile_level.second = AM_H264_PROFILE(air_cfg->profile);
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_AU_TYPE_EN_BIT)) {
        h264_cfg.au_type.first = true;
        h264_cfg.au_type.second = AM_H264_AU_TYPE(air_cfg->au_type);
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_M_EN_BIT)) {
        h264_cfg.M.first = true;
        h264_cfg.M.second = air_cfg->M;
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_N_EN_BIT)) {
        h264_cfg.N.first = true;
        h264_cfg.N.second = air_cfg->N;
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_IDR_EN_BIT)) {
        h264_cfg.idr_interval.first = true;
        h264_cfg.idr_interval.second = air_cfg->idr_interval;
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_BITRATE_EN_BIT)) {
        h264_cfg.target_bitrate.first = true;
        h264_cfg.target_bitrate.second = air_cfg->target_bitrate;
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_MV_THRESHOLD_EN_BIT)) {
        h264_cfg.mv_threshold.first = true;
        h264_cfg.mv_threshold.second = air_cfg->mv_threshold;
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_FLAT_AREA_IMPROVE_EN_BIT)) {
        h264_cfg.flat_area_improve.first = true;
        h264_cfg.flat_area_improve.second = air_cfg->flat_area_improve;
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_H264_CFG_MULTI_REF_P_EN_BIT)) {
        h264_cfg.multi_ref_p.first = true;
        h264_cfg.multi_ref_p.second = air_cfg->multi_ref_p;
      }

      if (TEST_BIT(air_cfg->enable_bits,
                   AM_H264_CFG_FAST_SEEK_INTVL_EN_BIT)) {
        h264_cfg.fast_seek_intvl.first = true;
        h264_cfg.fast_seek_intvl.second = air_cfg->fast_seek_intvl;
      }

      stream_param_map[AM_STREAM_ID(air_cfg->stream_id)] = stream_cfg;
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->set_stream_config(stream_param_map))) {
        ERROR("Failed to set stream configuration!");
        svc_ret->ret = -2;
        break;
      }
    } while(0);
  }
}

void ON_CFG_STREAM_H265_GET(void *msg_data,
                            int msg_data_size,
                            void *result_addr,
                            int result_max_size)
{
  WARN("TODO");
}

void ON_CFG_STREAM_H265_SET(void *msg_data,
                            int msg_data_size,
                            void *result_addr,
                            int result_max_size)
{
  WARN("TODO");
}

void ON_CFG_STREAM_MJPEG_GET(void *msg_data,
                             int msg_data_size,
                             void *result_addr,
                             int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Stream Config Get");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_mjpeg_cfg_t *air_cfg = (am_mjpeg_cfg_t*)msg_data;
    do {
      AMStreamParamMap stream_param_map;
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->get_stream_config(stream_param_map))) {
        ERROR("Failed to get stream configuration!");
        svc_ret->ret = -2;
        break;
      }
      AMStreamMJPEGConfig &mjpeg_cfg =
          stream_param_map[AM_STREAM_ID(air_cfg->stream_id)].mjpeg_config.second;

      /* MJpeg Config */
      air_cfg->quality = mjpeg_cfg.quality.second;
      air_cfg->chroma = mjpeg_cfg.chroma_format.second;
    } while(0);
  }
}

void ON_CFG_STREAM_MJPEG_SET(void *msg_data,
                             int msg_data_size,
                             void *result_addr,
                             int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Stream Config Set");

  if (AM_UNLIKELY(!msg_data)) {
    svc_ret->ret = -1;
    ERROR("Invalid parameter! msg_data is NULL!");
  } else {
    am_mjpeg_cfg_t *air_cfg = (am_mjpeg_cfg_t*)msg_data;
    do {
      AMStreamParamMap stream_param_map;
      AMStreamConfigParam stream_cfg;
      AMStreamMJPEGConfig &mjpeg_cfg = stream_cfg.mjpeg_config.second;

      /* MJpeg Config */
      if (TEST_BIT(air_cfg->enable_bits, AM_MJPEG_CFG_QUALITY_EN_BIT)) {
        mjpeg_cfg.quality.first = true;
        mjpeg_cfg.quality.second = air_cfg->quality;
      }

      if (TEST_BIT(air_cfg->enable_bits, AM_MJPEG_CFG_CHROMA_EN_BIT)) {
        mjpeg_cfg.chroma_format.first = true;
        mjpeg_cfg.chroma_format.second = AM_CHROMA_FORMAT(air_cfg->chroma);
      }

      stream_param_map[AM_STREAM_ID(air_cfg->stream_id)] = stream_cfg;
      if (AM_UNLIKELY(AM_RESULT_OK !=
          g_video_camera->set_stream_config(stream_param_map))) {
        ERROR("Failed to set stream configuration!");
        svc_ret->ret = -2;
        break;
      }
    } while(0);
  }
}

void ON_DYN_VOUT_HALT(void *msg_data,
                      int msg_data_size,
                      void *result_addr,
                      int result_max_size)
{
  do {
    am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
    memset(svc_ret, 0, sizeof(*svc_ret));
    INFO("Video Service: Vout Halt");
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }

    AM_VOUT_ID *id = (AM_VOUT_ID*) msg_data;
    if (AM_UNLIKELY(AM_RESULT_OK !=
        g_video_camera->halt_vout(*id))) {
      ERROR("Failed to stop vout!");
      svc_ret->ret = -1;
    }
  } while(0);

  return;
}

void ON_DYN_STREAM_MAX_NUM_GET(void *msg_data,
                               int msg_data_size,
                               void *result_addr,
                               int result_max_size)
{
  INFO("video service ON_DYN_STREAM_MAX_NUM_GET\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  uint32_t *val = (uint32_t*)svc_ret->data;
  *val = g_video_camera->get_encode_stream_max_num();

  svc_ret->ret = 0;
}

void ON_DYN_BUFFER_MAX_NUM_GET(void *msg_data,
                               int msg_data_size,
                               void *result_addr,
                               int result_max_size)
{
  INFO("video service ON_DYN_BUFFER_MAX_NUM_GET\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  uint32_t *val = (uint32_t*)svc_ret->data;
  *val = g_video_camera->get_source_buffer_max_num();

  svc_ret->ret = 0;
}

void ON_DYN_STREAM_STATUS_GET(void *msg_data,
                              int msg_data_size,
                              void *result_addr,
                              int result_max_size)
{
  INFO("Video Service: ON_DYN_STREAM_STATUS_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  if (!msg_data) {
    svc_ret->ret = -1;
    return;
  }
  am_stream_status_t *status = (am_stream_status_t*)svc_ret->data;
  uint32_t stream_num_max = g_video_camera->get_encode_stream_max_num();
  for (uint32_t id = AM_STREAM_ID_0; id < stream_num_max; ++id) {
    AM_STREAM_STATE state;
    if (g_video_camera->get_stream_status(AM_STREAM_ID(id),
                                          state) == AM_RESULT_OK) {
      if (state == AM_STREAM_STATE_ENCODING) {
        status->status |= 1 << id;
      }
    }
  }
}

void ON_DYN_STREAM_BITRATE_GET(void *msg_data,
                               int msg_data_size,
                               void *result_addr,
                               int result_max_size)
{
  INFO("Video Service: ON_DYN_STREAM_BITRATE_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMBitrate br;
    uint32_t stream_id = *((uint32_t*)msg_data);
    am_bitrate_t *bitrate = (am_bitrate_t *)svc_ret->data;
    br.stream_id = AM_STREAM_ID(stream_id);
    if (g_video_camera->get_bitrate(br) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    bitrate->target_bitrate = br.target_bitrate;
    bitrate->rate_control_mode = AM_H26X_RATE_CONTROL(br.rate_control_mode);
  } while (0);
}

void ON_DYN_STREAM_BITRATE_SET(void *msg_data,
                               int msg_data_size,
                               void *result_addr,
                               int result_max_size)
{
  INFO("Video Service: ON_DYN_STREAM_BITRATE_SET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMBitrate br;
    am_bitrate_t *bitrate = (am_bitrate_t *)msg_data;
    if (TEST_BIT(bitrate->enable_bits, AM_BITRATE_EN_BIT)) {
      br.stream_id = AM_STREAM_ID(bitrate->stream_id);
      br.rate_control_mode = AM_H26X_RATE_CONTROL(bitrate->rate_control_mode);
      br.target_bitrate = bitrate->target_bitrate;
    }

    if (g_video_camera->set_bitrate(br) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
  } while (0);
}

void ON_DYN_STREAM_FRAMEFACTOR_GET(void *msg_data,
                                   int msg_data_size,
                                   void *result_addr,
                                   int result_max_size)
{
  INFO("Video Service: ON_DYN_STREAM_FRAMEFACTOR_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMFrameFactor ff;
    uint32_t stream_id = *((uint32_t*)msg_data);
    am_framefactor_t *factor = (am_framefactor_t *)svc_ret->data;
    ff.stream_id = AM_STREAM_ID(stream_id);
    if (g_video_camera->get_framefactor(ff) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    factor->mul = ff.fps.mul;
    factor->div = ff.fps.div;
  } while (0);
}

void ON_DYN_STREAM_FRAMEFACTOR_SET(void *msg_data,
                                   int msg_data_size,
                                   void *result_addr,
                                   int result_max_size)
{
  INFO("Video Service: ON_DYN_STREAM_FRAMEFACTOR_SET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMFrameFactor ff;
    am_framefactor_t *factor = (am_framefactor_t *)msg_data;
    if (TEST_BIT(factor->enable_bits, AM_FRAMEFACTOR_EN_BIT)) {
      ff.stream_id = AM_STREAM_ID(factor->stream_id);
      ff.fps.mul = factor->mul;
      ff.fps.div = factor->div;
    }

    if (g_video_camera->set_framefactor(ff) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
  } while (0);
}

void ON_DYN_MJPEG_QUALITY_GET(void *msg_data,
                                   int msg_data_size,
                                   void *result_addr,
                                   int result_max_size)
{
  INFO("Video Service: ON_DYN_MJPEG_QUALITY_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMMJpegInfo info;
    uint32_t stream_id = *((uint32_t*)msg_data);
    am_mjpeg_cfg_t *mjpeg = (am_mjpeg_cfg_t *)svc_ret->data;
    info.stream_id = AM_STREAM_ID(stream_id);
    if (g_video_camera->get_mjpeg_info(info) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    mjpeg->quality = info.quality;
  } while (0);
}

void ON_DYN_MJPEG_QUALITY_SET(void *msg_data,
                              int msg_data_size,
                              void *result_addr,
                              int result_max_size)
{
  INFO("Video Service: ON_DYN_MJPEG_QUALITY_SET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMMJpegInfo info;
    am_mjpeg_cfg_t *mjpeg = (am_mjpeg_cfg_t *)msg_data;
    if (TEST_BIT(mjpeg->enable_bits, AM_MJPEG_CFG_QUALITY_EN_BIT)) {
      info.stream_id = AM_STREAM_ID(mjpeg->stream_id);
      info.quality = mjpeg->quality;
    }

    if (g_video_camera->set_mjpeg_info(info) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
  } while (0);

}

void ON_DYN_H264_GOP_GET(void *msg_data,
                         int msg_data_size,
                         void *result_addr,
                         int result_max_size)
{
  INFO("Video Service: ON_DYN_H264_GOP_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMH264GOP info;
    uint32_t stream_id = *((uint32_t*)msg_data);
    am_h264_cfg_t *h264 = (am_h264_cfg_t *)svc_ret->data;
    info.stream_id = AM_STREAM_ID(stream_id);
    if (g_video_camera->get_h264_gop(info) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    h264->N = info.N;
    h264->idr_interval = info.idr;
  } while (0);
}

void ON_DYN_H264_GOP_SET(void *msg_data,
                         int msg_data_size,
                         void *result_addr,
                         int result_max_size)
{
  INFO("Video Service: ON_DYN_H264_GOP_SET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMH264GOP info;
    am_h264_cfg_t *h264 = (am_h264_cfg_t *)msg_data;
    info.stream_id = AM_STREAM_ID(h264->stream_id);
    if (g_video_camera->get_h264_gop(info) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    if (TEST_BIT(h264->enable_bits, AM_H264_CFG_N_EN_BIT)) {
      info.N = h264->N;
    }
    if (TEST_BIT(h264->enable_bits, AM_H264_CFG_IDR_EN_BIT)) {
      info.idr = h264->idr_interval;
    }

    if (g_video_camera->set_h264_gop(info) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
  } while (0);
}

void ON_DYN_STREAM_OFFSET_GET(void *msg_data,
                              int msg_data_size,
                              void *result_addr,
                              int result_max_size)
{
  INFO("Video Service: ON_DYN_STREAM_OFFSET_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMOffset offset;
    AM_STREAM_ID stream_id = *((AM_STREAM_ID *)msg_data);
    am_stream_offset_t *poffset = (am_stream_offset_t *)svc_ret->data;
    if (g_video_camera->get_stream_offset(stream_id, offset) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    poffset->x = offset.x;
    poffset->y = offset.y;
  } while (0);
}

void ON_DYN_STREAM_OFFSET_SET(void *msg_data,
                              int msg_data_size,
                              void *result_addr,
                              int result_max_size)
{
  INFO("Video Service: ON_DYN_STREAM_OFFSET_SET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  do {
    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    AMOffset offset;
    am_stream_offset_t *poffset = (am_stream_offset_t *)msg_data;
    if (g_video_camera->get_stream_offset(AM_STREAM_ID(poffset->stream_id), offset)
        != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    if (TEST_BIT(poffset->enable_bits, AM_STREAM_OFFSET_X_EN_BIT)) {
      offset.x = poffset->x;
    }
    if (TEST_BIT(poffset->enable_bits, AM_STREAM_OFFSET_Y_EN_BIT)) {
      offset.y = poffset->y;
    }

    if (g_video_camera->set_stream_offset(AM_STREAM_ID(poffset->stream_id), offset)
        != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
  } while (0);

}

void ON_VIN_STOP(void *msg_data,
                 int msg_data_size,
                 void *result_addr,
                 int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Vin Stop");
  if (AM_UNLIKELY(AM_RESULT_OK !=
      g_video_camera->stop_vin())) {
    ERROR("Failed to stop vin!");
    svc_ret->ret = -2;
  }

  return;
}

void ON_DYN_DPTZ_RATIO_SET(void *msg_data,
                           int msg_data_size,
                           void *result_addr,
                           int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("video service ON_DYN_DPTZ_RATIO_SET");

  do {
    AMIDPTZ *dptz = (AMIDPTZ*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_DPTZ);
    if (!dptz) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_DPTZ);
      break;
    }
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }

    am_dptz_ratio_t *param = (am_dptz_ratio_t*) msg_data;

    AMDPTZRatio ratio;
    AM_SOURCE_BUFFER_ID id = AM_SOURCE_BUFFER_ID(param->buffer_id);

    if (TEST_BIT(param->enable_bits, AM_DPTZ_PAN_RATIO_EN_BIT)) {
      ratio.pan.first = true;
      ratio.pan.second = param->pan_ratio;
    }

    if (TEST_BIT(param->enable_bits, AM_DPTZ_TILT_RATIO_EN_BIT)) {
      ratio.tilt.first = true;
      ratio.tilt.second = param->tilt_ratio;
    }

    if (TEST_BIT(param->enable_bits, AM_DPTZ_ZOOM_RATIO_EN_BIT)) {
      ratio.zoom.first = true;
      ratio.zoom.second = param->zoom_ratio;
    }

    if (dptz->set_ratio(id, ratio) == false) {
      ERROR("set ratio failed!\n");
      svc_ret->ret = AM_RESULT_ERR_INVALID;
      break;
    }
  } while (0);
}

void ON_DYN_DPTZ_RATIO_GET(void *msg_data,
                           int msg_data_size,
                           void *result_addr,
                           int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  am_dptz_ratio_t *dptz_param = (am_dptz_ratio_t*) svc_ret->data;
  AMDPTZRatio ratio;
  INFO("video service ON_DYN_DPTZ_RATIO_GET");

  do {
    AMIDPTZ *dptz = (AMIDPTZ*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_DPTZ);
    if (!dptz) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_DPTZ);
      break;
    }
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }
    AM_SOURCE_BUFFER_ID id = AM_SOURCE_BUFFER_ID(*((uint32_t*) msg_data));
    if (dptz->get_ratio(id, ratio) == false) {
      ERROR("get ratio failed!\n");
      svc_ret->ret = AM_RESULT_ERR_INVALID;
      break;
    }

    dptz_param->buffer_id = id;
    dptz_param->pan_ratio = ratio.pan.second;
    dptz_param->tilt_ratio = ratio.tilt.second;
    dptz_param->zoom_ratio = ratio.zoom.second;
  } while (0);
}

void ON_DYN_DPTZ_SIZE_SET(void *msg_data,
                          int msg_data_size,
                          void *result_addr,
                          int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("video service ON_DYN_DPTZ_SIZE_SET");

  do {
    AMIDPTZ *dptz = (AMIDPTZ*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_DPTZ);
    if (!dptz) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_DPTZ);
      break;
    }
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }

    am_dptz_size_t *param = (am_dptz_size_t*) msg_data;

    AMDPTZSize rect;
    AM_SOURCE_BUFFER_ID id = AM_SOURCE_BUFFER_ID(param->buffer_id);

    if (TEST_BIT(param->enable_bits, AM_DPTZ_SIZE_X_EN_BIT)) {
      rect.x.first = true;
      rect.x.second = param->x;
    }

    if (TEST_BIT(param->enable_bits, AM_DPTZ_SIZE_Y_EN_BIT)) {
      rect.y.first = true;
      rect.y.second = param->y;
    }

    if (TEST_BIT(param->enable_bits, AM_DPTZ_SIZE_W_EN_BIT)) {
      rect.w.first = true;
      rect.w.second = param->w;
    }

    if (TEST_BIT(param->enable_bits, AM_DPTZ_SIZE_H_EN_BIT)) {
      rect.h.first = true;
      rect.h.second = param->h;
    }

    if (dptz->set_size(id, rect) == false) {
      ERROR("set ratio failed!\n");
      svc_ret->ret = AM_RESULT_ERR_INVALID;
      break;
    }
  } while (0);
}

void ON_DYN_DPTZ_SIZE_GET(void *msg_data,
                          int msg_data_size,
                          void *result_addr,
                          int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  am_dptz_size_t *dptz_param = (am_dptz_size_t*) svc_ret->data;
  AMDPTZSize size;
  INFO("video service ON_DYN_DPTZ_SIZE_GET");

  do {
    AMIDPTZ *dptz = (AMIDPTZ*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_DPTZ);
    if (!dptz) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_DPTZ);
      break;
    }
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }
    AM_SOURCE_BUFFER_ID id = AM_SOURCE_BUFFER_ID(*((uint32_t*) msg_data));
    if (dptz->get_size(id, size) == false) {
      ERROR("get size failed!\n");
      svc_ret->ret = AM_RESULT_ERR_INVALID;
      break;
    }

    dptz_param->w = size.w.second;
    dptz_param->h = size.h.second;
    dptz_param->x = size.x.second;
    dptz_param->y = size.y.second;
  } while (0);

}

void ON_DYN_WARP_SET(void *msg_data,
                     int msg_data_size,
                     void *result_addr,
                     int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("video service ON_WARP_SET!\n");

  do {
    am_warp_t *warp_param = nullptr;
    AMIEncodeWarp *warp =
        (AMIEncodeWarp*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_WARP);
    float ldc_strength = 0.0;
    float pano_hfov_degree = 0.0;
    int warp_region_yaw = 0;
    svc_ret->ret = -1;
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer!\n");
      break;
    }

    if (AM_UNLIKELY(!warp)) {
      WARN("Video Plugin %s is not loaded!", VIDEO_PLUGIN_WARP_SO);
      break;
    }

    warp_param = (am_warp_t*)msg_data;
    if (TEST_BIT(warp_param->enable_bits, AM_WARP_LDC_STRENGTH_EN_BIT)) {
      if (AM_RESULT_OK != warp->get_ldc_strength(ldc_strength)) {
        break;
      }
      if (AM_RESULT_OK != warp->set_ldc_strength(warp_param->ldc_strength)) {
        break;
      }
    }

    if (TEST_BIT(warp_param->enable_bits, AM_WARP_PANO_HFOV_DEGREE_EN_BIT)) {
      if (AM_RESULT_OK != warp->get_pano_hfov_degree(pano_hfov_degree)) {
        break;
      }
      if (AM_RESULT_OK !=
          warp->set_pano_hfov_degree(warp_param->pano_hfov_degree)) {
        break;
      }
    }

    if (TEST_BIT(warp_param->enable_bits, AM_WARP_REGION_YAW_EN_BIT)) {
      if (AM_RESULT_OK != warp->get_warp_region_yaw(warp_region_yaw)) {
        break;
      }
      if (AM_RESULT_OK !=
          warp->set_warp_region_yaw(warp_param->warp_region_yaw)) {
        break;
      }
    }

    if (AM_RESULT_OK != warp->apply()) {
      ERROR("Failed to apply warp parameters! Reset to last one!");
      if (TEST_BIT(warp_param->enable_bits, AM_WARP_LDC_STRENGTH_EN_BIT)) {
        if (AM_RESULT_OK != warp->set_ldc_strength(ldc_strength)) {
          break;
        }
      }
      if (TEST_BIT(warp_param->enable_bits, AM_WARP_PANO_HFOV_DEGREE_EN_BIT)) {
        if (AM_RESULT_OK != warp->set_pano_hfov_degree(pano_hfov_degree)) {
          break;
        }
      }
      if (TEST_BIT(warp_param->enable_bits, AM_WARP_REGION_YAW_EN_BIT)) {
        if (AM_RESULT_OK != warp->set_warp_region_yaw(warp_region_yaw)) {
          break;
        }
      }
      break;
    }
    svc_ret->ret = 0;
  } while (0);
}

void ON_DYN_WARP_GET(void *msg_data,
                     int msg_data_size,
                     void *result_addr,
                     int result_max_size)
{
  INFO("video service ON_WARP_GET");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  am_warp_t *warp_param = (am_warp_t*) svc_ret->data;

  do {
    AMIEncodeWarp *warp =
        (AMIEncodeWarp*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_WARP);

    svc_ret->ret = -1;
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer!\n");
      break;
    }

    if (AM_UNLIKELY(!warp)) {
      WARN("Video Plugin %s is not loaded!", VIDEO_PLUGIN_WARP_SO);
      break;
    }

    if (AM_RESULT_OK != warp->get_ldc_strength(warp_param->ldc_strength)) {
      ERROR("Failed to get warp LDC strength!");
      break;
    }

    if (AM_RESULT_OK !=
        warp->get_pano_hfov_degree(warp_param->pano_hfov_degree)) {
      ERROR("Failed to get warp pano hfov degree!");
      break;
    }

    svc_ret->ret = 0;
  } while (0);
}

void ON_DYN_LBR_SET(void *msg_data,
                    int msg_data_size,
                    void *result_addr,
                    int result_max_size)
{
  INFO("video service ON_ENCODE_H264_LBR_SET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));
  svc_ret->ret = 0;

  do {
    am_encode_h264_lbr_ctrl_t *lbr_param = (am_encode_h264_lbr_ctrl_t*)msg_data;
    AMILBRControl *lbr =
        (AMILBRControl*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_LBR);

    if (AM_UNLIKELY(!lbr_param)) {
      ERROR("Invalid data pointer!");
      break;
    }

    if (AM_UNLIKELY(!lbr)) {
      WARN("Video Plugin %s is not loaded!", VIDEO_PLUGIN_LBR_SO);
      break;
    }

    if (TEST_BIT(lbr_param->enable_bits,
                 AM_ENCODE_H264_LBR_ENABLE_LBR_EN_BIT)) {
      lbr->set_enable(lbr_param->stream_id, lbr_param->enable_lbr);
    }
    if (TEST_BIT(lbr_param->enable_bits,
                 AM_ENCODE_H264_LBR_AUTO_BITRATE_CEILING_EN_BIT)) {
      uint32_t ceiling = 0;
      bool is_auto = false;
      lbr->get_bitrate_ceiling(lbr_param->stream_id, ceiling, is_auto);

      if (is_auto != lbr_param->auto_bitrate_ceiling) {
        if (TEST_BIT(lbr_param->enable_bits,
                     AM_ENCODE_H264_LBR_BITRATE_CEILING_EN_BIT)) {
          lbr->set_bitrate_ceiling(lbr_param->stream_id,
                                   lbr_param->bitrate_ceiling,
                                   lbr_param->auto_bitrate_ceiling);
        } else {
          lbr->set_bitrate_ceiling(lbr_param->stream_id,
                                   ceiling,
                                   lbr_param->auto_bitrate_ceiling);
        }
      }
    }
    if (TEST_BIT(lbr_param->enable_bits,
                 AM_ENCODE_H264_LBR_BITRATE_CEILING_EN_BIT)) {
      uint32_t ceiling = 0;
      bool is_auto = false;
      lbr->get_bitrate_ceiling(lbr_param->stream_id, ceiling, is_auto);
      lbr->set_bitrate_ceiling(lbr_param->stream_id,
                               lbr_param->bitrate_ceiling,
                               is_auto);
    }
    if (TEST_BIT(lbr_param->enable_bits,
                 AM_ENCODE_H264_LBR_DROP_FRAME_EN_BIT)) {
      lbr->set_drop_frame_enable(lbr_param->stream_id, lbr_param->drop_frame);
    }
  } while(0);
}

void ON_DYN_LBR_GET(void *msg_data,
                    int msg_data_size,
                    void *result_addr,
                    int result_max_size)
{
  INFO("video service ON_ENCODE_H264_LBR_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(*svc_ret));

  svc_ret->ret = 0;

  do {
    am_encode_h264_lbr_ctrl_t *lbr_param =
        (am_encode_h264_lbr_ctrl_t*)svc_ret->data;
    uint32_t stream_id = *((uint32_t*)msg_data);
    AMILBRControl *lbr =
        (AMILBRControl*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_LBR);

    if (AM_UNLIKELY(!lbr_param)) {
      ERROR("Invalid data pointer!");
      break;
    }

    if (AM_UNLIKELY(!lbr)) {
      WARN("Video Plugin %s is not loaded!", VIDEO_PLUGIN_LBR_SO);
      break;
    }
    lbr_param->stream_id = stream_id;
    lbr_param->enable_lbr = lbr->get_enable(stream_id);
    lbr_param->drop_frame = lbr->get_drop_frame_enable(stream_id);
    lbr->get_bitrate_ceiling(stream_id, lbr_param->bitrate_ceiling,
                             lbr_param->auto_bitrate_ceiling);
  } while(0);
}

void ON_VIDEO_ENCODE_START(void *msg_data,
                           int msg_data_size,
                           void *result_addr,
                           int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Video Encode Start");

  if (AM_UNLIKELY(AM_RESULT_OK != g_video_camera->start())) {
    svc_ret->ret = -1;
    ERROR("Video Service: Failed to start encoding!");
  }
}

void ON_VIDEO_ENCODE_STOP(void *msg_data,
                          int msg_data_size,
                          void *result_addr,
                          int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;

  memset(svc_ret, 0, sizeof(*svc_ret));
  INFO("Video Service: Video Encode Stop");

  if (AM_UNLIKELY(AM_RESULT_OK != g_video_camera->stop())) {
    svc_ret->ret = -1;
    ERROR("Video Service: Failed to stop encoding!");
  }
}

void ON_VIDEO_DYN_FORCE_IDR(void *msg_data,
                            int msg_data_size,
                            void *result_addr,
                            int result_max_size)
{
  INFO("video service ON_VIDEO_FORCE_IDR\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  if (!msg_data) {
    ERROR("NULL pointer!\n");
    svc_ret->ret = -1;
    return;
  }
  AM_STREAM_ID id = *(AM_STREAM_ID*)msg_data;
  if (g_video_camera->force_idr(id) != AM_RESULT_OK) {
    svc_ret->ret = -1;
  }
}

void ON_COMMON_GET_EVENT(void *msg_data,
                         int msg_data_size,
                         void *result_addr,
                         int result_max_size)
{
  INFO("video service ON_COMMON_GET_EVENT!\n");
  do {
    AMILBRControl *lbr =
        (AMILBRControl*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_LBR);
    if (lbr) {
      lbr->process_motion_info(msg_data);
    }

  } while (0);
}

void ON_VIDEO_OVERLAY_GET_MAX_NUM(void *msg_data,
                                  int msg_data_size,
                                  void *result_addr,
                                  int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_GET_MAX_NUM\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));


  AMIEncodeOverlay *ol =
      (AMIEncodeOverlay*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_OVERLAY);
  if (ol) {
    AMOverlayUserDefLimitVal user_val;
    am_overlay_limit_val_t *limit = (am_overlay_limit_val_t*)svc_ret->data;

    ol->get_user_defined_limit_value(user_val);
    limit->platform_stream_num_max = g_video_camera->
        get_encode_stream_max_num();
    limit->platform_overlay_area_num_max = ol->get_area_max_num();
    limit->user_def_stream_num_max = user_val.s_num_max.second;
    limit->user_def_overlay_area_num_max = user_val.a_num_max.second;
  }

  svc_ret->ret = 0;
}

void ON_VIDEO_OVERLAY_DESTROY(void *msg_data,
                              int msg_data_size,
                              void *result_addr,
                              int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_DESTROY\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  AMIEncodeOverlay *ol =
      (AMIEncodeOverlay*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_OVERLAY);
  if (ol) {
    svc_ret->ret = (ol->destroy_overlay() != AM_RESULT_OK) ? -1 : 0;
  } else {
    NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
  }
}

void ON_VIDEO_OVERLAY_SAVE(void *msg_data,
                           int msg_data_size,
                           void *result_addr,
                           int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_SAVE\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  AMIEncodeOverlay *ol =
      (AMIEncodeOverlay*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_OVERLAY);

  if (ol) {
    if (AM_RESULT_OK != ol->save_param_to_config()) {
      svc_ret->ret = -1;
    }
  } else {
    NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
  }
}

void ON_VIDEO_OVERLAY_INIT(void *msg_data,
                           int msg_data_size,
                           void *result_addr,
                           int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_INIT!\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  do {
    AMIEncodeOverlay *ol = (AMIEncodeOverlay*)g_video_camera->\
              get_video_plugin(VIDEO_PLUGIN_OVERLAY);
    if (!ol) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
      break;
    }

    if (!msg_data) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }
    int32_t *area_id = (int32_t *)svc_ret->data;
    *area_id = -1;
    am_overlay_area_t *param = (am_overlay_area_t*) msg_data;

    if (TEST_BIT(param->enable_bits, AM_OVERLAY_INIT_EN_BIT)) {
      AMOverlayAreaAttr attr;
      attr.enable = 0;
      AM_STREAM_ID stream_id = AM_STREAM_ID(param->stream_id);

      AMRect &rect = attr.rect;
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_RECT_EN_BIT)) {
        rect.size.width = param->width;
        rect.size.height = param->height;
        rect.offset.x = param->offset_x;
        rect.offset.y = param->offset_y;
      }

      if (TEST_BIT(param->enable_bits, AM_OVERLAY_ROTATE_EN_BIT)) {
        attr.rotate = param->rotate;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_BUF_NUM_EN_BIT)) {
        attr.buf_num = param->buf_num;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_BG_COLOR_EN_BIT)) {
        AMOverlayCLUT &clut = attr.bg_color;
        clut.v = uint8_t((param->bg_color >> 24) & 0xff);
        clut.u = uint8_t((param->bg_color >> 16) & 0xff);
        clut.y = uint8_t((param->bg_color >> 8) & 0xff);
        clut.a = uint8_t(param->bg_color & 0xff);
      }

      *area_id = ol->init_area(stream_id, attr);
      if (*area_id < 0) {
        ERROR("Video Service: failed to init a overlay area for stream%d",
              stream_id);
        svc_ret->ret = -1;
      }
    }
  } while(0);

  return;
}

void ON_VIDEO_OVERLAY_DATA_ADD(void *msg_data,
                               int msg_data_size,
                               void *result_addr,
                               int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_DATA_ADD!\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  do {
    AMIEncodeOverlay *ol = (AMIEncodeOverlay*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_OVERLAY);
    if (!ol) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
      break;
    }

    if (!msg_data) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }

    int32_t *data_index = (int32_t *)svc_ret->data;
    *data_index = -1;
    am_overlay_data_t *param = (am_overlay_data_t*) msg_data;
    if (TEST_BIT(param->enable_bits, AM_OVERLAY_DATA_ADD_EN_BIT)) {
      AM_STREAM_ID stream_id = AM_STREAM_ID(param->id.stream_id);
      int32_t area_id = param->id.area_id;
      AMOverlayAreaData data;

      AMRect &rect = data.rect;
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_RECT_EN_BIT)) {
        rect.size.width = param->width;
        rect.size.height = param->height;
        rect.offset.x = param->offset_x;
        rect.offset.y = param->offset_y;
      }

      if (TEST_BIT(param->enable_bits, AM_OVERLAY_DATA_TYPE_EN_BIT)) {
        data.type = AM_OVERLAY_DATA_TYPE(param->type);
      }

      if ((AM_OVERLAY_DATA_TYPE_STRING == data.type) ||
          (AM_OVERLAY_DATA_TYPE_TIME == data.type)) {
        AMOverlayTextBox &text = data.text;

        if (TEST_BIT(param->enable_bits, AM_OVERLAY_BG_COLOR_EN_BIT)) {
          AMOverlayCLUT &clut = text.background_color;
          clut.v = uint8_t((param->bg_color >> 24) & 0xff);
          clut.u = uint8_t((param->bg_color >> 16) & 0xff);
          clut.y = uint8_t((param->bg_color >> 8) & 0xff);
          clut.a = uint8_t(param->bg_color & 0xff);
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_STRING_EN_BIT)) {
          text.str = param->str;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_TIME_EN_BIT)) {
          text.pre_str = param->pre_str;
          text.suf_str = param->suf_str;
          text.en_msec = param->msec_en;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_STRING_EN_BIT)) {
          text.spacing = param->spacing;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_COLOR_EN_BIT)) {
          if (param->font_color < 8) {
            text.font_color.id = param->font_color;
          } else {
            text.font_color.id = 8;
            AMOverlayCLUT &clut = text.font_color.color;
            clut.v = uint8_t((param->font_color >> 24) & 0xff);
            clut.u = uint8_t((param->font_color >> 16) & 0xff);
            clut.y = uint8_t((param->font_color >> 8) & 0xff);
            clut.a = uint8_t(param->font_color & 0xff);
          }
        }

        AMOverlayFont &font = text.font;
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_TYPE_EN_BIT)) {
          font.ttf_name = param->font_type;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_SIZE_EN_BIT)) {
          font.width = param->font_size;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_OUTLINE_EN_BIT)) {
          font.outline_width = param->font_outline_w;

          AMOverlayCLUT &clut = text.outline_color;
          clut.v = uint8_t((param->font_outline_color >> 24) & 0xff);
          clut.u = uint8_t((param->font_outline_color >> 16) & 0xff);
          clut.y = uint8_t((param->font_outline_color >> 8) & 0xff);
          clut.a = uint8_t(param->font_outline_color & 0xff);
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_BOLD_EN_BIT)) {
          font.hor_bold = param->font_hor_bold;
          font.ver_bold = param->font_ver_bold;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_ITALIC_EN_BIT)) {
          font.italic = param->font_italic;
        }
      } else if (AM_OVERLAY_DATA_TYPE_PICTURE == data.type ||
          AM_OVERLAY_DATA_TYPE_ANIMATION == data.type) {
        AMOverlayPicture &pic = data.pic;

        if (TEST_BIT(param->enable_bits, AM_OVERLAY_BMP_EN_BIT)) {
          pic.filename = param->bmp;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_BMP_COLOR_EN_BIT))
        {
          AMOverlayCLUT &clut = pic.colorkey.color;
          clut.v = uint8_t((param->color_key  >> 24) & 0xff);
          clut.u = uint8_t((param->color_key  >> 16) & 0xff);
          clut.y = uint8_t((param->color_key  >> 8) & 0xff);
          clut.a = uint8_t(param->color_key & 0xff);

          pic.colorkey.range = param->color_range;
        }
        if (TEST_BIT(param->enable_bits, AM_OVERLAY_ANIMATION_EN_BIT)) {
          pic.num = param->bmp_num;
          pic.interval = param->interval;
        }
      }

      if ((*data_index = ol->add_data_to_area(stream_id, area_id, data)) < 0) {
        ERROR("Video Service: failed to add a data block to overlay area");
        svc_ret->ret = -1;
      }
    }
  } while(0);

  return;
}

void ON_VIDEO_OVERLAY_DATA_UPDATE(void *msg_data,
                                  int msg_data_size,
                                  void *result_addr,
                                  int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_DATA_UPDATE!\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  do {
    AMIEncodeOverlay *ol = (AMIEncodeOverlay*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_OVERLAY);

    if (!ol) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
      break;
    }

    if (!msg_data) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }

    am_overlay_data_t *param = (am_overlay_data_t*) msg_data;
    if (TEST_BIT(param->enable_bits, AM_OVERLAY_DATA_UPDATE_EN_BIT)) {
      AM_STREAM_ID stream_id = AM_STREAM_ID(param->id.stream_id);
      int32_t area_id = param->id.area_id;
      int32_t index = param->id.data_index;
      AMOverlayAreaData data;

      AMOverlayTextBox &text = data.text;
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_BG_COLOR_EN_BIT)) {
        AMOverlayCLUT &clut = text.background_color;
        clut.v = uint8_t((param->bg_color >> 24) & 0xff);
        clut.u = uint8_t((param->bg_color >> 16) & 0xff);
        clut.y = uint8_t((param->bg_color >> 8) & 0xff);
        clut.a = uint8_t(param->bg_color & 0xff);
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_STRING_EN_BIT)) {
        text.str = param->str;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_STRING_EN_BIT)) {
        text.spacing = param->spacing;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_COLOR_EN_BIT)) {
        if (param->font_color < 8) {
          text.font_color.id = param->font_color;
        } else {
          text.font_color.id = 8;
          AMOverlayCLUT &clut = text.font_color.color;
          clut.v = uint8_t((param->font_color >> 24) & 0xff);
          clut.u = uint8_t((param->font_color >> 16) & 0xff);
          clut.y = uint8_t((param->font_color >> 8) & 0xff);
          clut.a = uint8_t(param->font_color & 0xff);
        }
      }

      AMOverlayFont &font = text.font;
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_TYPE_EN_BIT)) {
        font.ttf_name = param->font_type;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_SIZE_EN_BIT)) {
        font.width = param->font_size;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_OUTLINE_EN_BIT)) {
        font.outline_width = param->font_outline_w;

        AMOverlayCLUT &clut = text.outline_color;
        clut.v = uint8_t((param->font_outline_color >> 24) & 0xff);
        clut.u = uint8_t((param->font_outline_color >> 16) & 0xff);
        clut.y = uint8_t((param->font_outline_color >> 8) & 0xff);
        clut.a = uint8_t(param->font_outline_color & 0xff);
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_BOLD_EN_BIT)) {
        font.hor_bold = param->font_hor_bold;
        font.ver_bold = param->font_ver_bold;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_FONT_ITALIC_EN_BIT)) {
        font.italic = param->font_italic;
      }

      AMOverlayPicture &pic = data.pic;
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_BMP_EN_BIT)) {
        pic.filename = param->bmp;
      }
      if (TEST_BIT(param->enable_bits, AM_OVERLAY_BMP_COLOR_EN_BIT))
      {
        AMOverlayCLUT &clut = pic.colorkey.color;
        clut.v = uint8_t((param->color_key  >> 24) & 0xff);
        clut.u = uint8_t((param->color_key  >> 16) & 0xff);
        clut.y = uint8_t((param->color_key  >> 8) & 0xff);
        clut.a = uint8_t(param->color_key & 0xff);

        pic.colorkey.range = param->color_range;
      }

      if (ol->update_area_data(stream_id, area_id,
                               index, data) != AM_RESULT_OK) {
        ERROR("Video Service: failed to update the overlay area data block");
        svc_ret->ret = -1;
      }
    }
  } while(0);

  return;
}

void ON_VIDEO_OVERLAY_SET(void *msg_data,
                          int msg_data_size,
                          void *result_addr,
                          int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_SET\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  do {
    AMIEncodeOverlay *ol = (AMIEncodeOverlay*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_OVERLAY);

    if (!ol) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
      break;
    }

    if (!msg_data) {
      svc_ret->ret = -1;
      break;
    }
    am_overlay_id_t *id = (am_overlay_id_t*)msg_data;

    //remove area
    if (TEST_BIT(id->enable_bits, AM_OVERLAY_REMOVE_EN_BIT)) {
      AM_STREAM_ID stream_id = AM_STREAM_ID(id->stream_id);
      int32_t area_id = id->area_id;
      if (ol->change_state(stream_id, area_id, AM_OVERLAY_DELETE) !=
          AM_RESULT_OK) {
        ERROR("Video Service: failed to remove overlay area\n");
        svc_ret->ret = -1;
        break;
      }
    }

    //enable
    if (TEST_BIT(id->enable_bits, AM_OVERLAY_ENABLE_EN_BIT)) {
      AM_STREAM_ID stream_id = AM_STREAM_ID(id->stream_id);
      int32_t area_id = id->area_id;
      if (ol->change_state(stream_id, area_id, AM_OVERLAY_ENABLE) !=
          AM_RESULT_OK) {
        ERROR("Video Service: failed to enable overlay area\n");
        svc_ret->ret = -1;
        break;
      }
    }

    //disable
    if (TEST_BIT(id->enable_bits, AM_OVERLAY_DISABLE_EN_BIT)) {
      AM_STREAM_ID stream_id = AM_STREAM_ID(id->stream_id);
      int32_t area_id = id->area_id;
      if (ol->change_state(stream_id, area_id, AM_OVERLAY_DISABLE) !=
          AM_RESULT_OK) {
        ERROR("Video Service: failed to disable overlay area\n");
        svc_ret->ret = -1;
        break;
      }
    }

    //remove data block
    if (TEST_BIT(id->enable_bits, AM_OVERLAY_DATA_REMOVE_EN_BIT)) {
      AM_STREAM_ID stream_id = AM_STREAM_ID(id->stream_id);
      int32_t area_id = id->area_id;
      int32_t index = id->data_index;
      if (ol->delete_area_data(stream_id, area_id, index) !=
          AM_RESULT_OK) {
        ERROR("Video Service: failed to remove overlay area data block\n");
        svc_ret->ret = -1;
        break;
      }
    }
  } while(0);

  return;
}

void ON_VIDEO_OVERLAY_GET(void *msg_data,
                          int msg_data_size,
                          void *result_addr,
                          int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_GET!\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  do {
    AMIEncodeOverlay *ol = (AMIEncodeOverlay*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_OVERLAY);

    if (!ol) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
      break;
    }

    if (!msg_data) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      break;
    }

    am_overlay_area_t *area = (am_overlay_area_t*)svc_ret->data;
    AM_STREAM_ID stream_id = AM_STREAM_ID(((am_overlay_id_t*)msg_data)
                                          ->stream_id);
    uint32_t area_id = ((am_overlay_id_t*)msg_data)->area_id;

    AMOverlayAreaParam param;
    if (ol->get_area_param(stream_id, area_id, param) != AM_RESULT_OK) {
      svc_ret->ret = -1;
      break;
    }
    if (param.attr.enable < 0) {
     area->enable = 404;
     break;
    }

    area->enable = param.attr.enable;
    area->width = param.attr.rect.size.width;
    area->height = param.attr.rect.size.height;
    area->offset_x = param.attr.rect.offset.x;
    area->offset_y = param.attr.rect.offset.y;
    area->rotate = param.attr.rotate;
    AMOverlayCLUT &c = param.attr.bg_color;
    area->bg_color = (c.v << 24) | (c.u << 16) | (c.y << 8) | c.a;
    area->buf_num = param.attr.buf_num;
    area->num = param.num;
  } while(0);

  return;
}

void ON_VIDEO_OVERLAY_DATA_GET(void *msg_data,
                               int msg_data_size,
                               void *result_addr,
                               int result_max_size)
{
  INFO("video service ON_VIDEO_OVERLAY_DATA_GET!\n");
  am_service_result_t *svc_ret = (am_service_result_t*) result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));

  do {
    AMIEncodeOverlay *ol = (AMIEncodeOverlay*)g_video_camera->\
        get_video_plugin(VIDEO_PLUGIN_OVERLAY);

    if (!ol) {
      NOTICE("Video Plugin \"%s\" is not loaded!", VIDEO_PLUGIN_OVERLAY);
      break;
    }

    if (!msg_data) {
      ERROR("NULL pointer!\n");
      svc_ret->ret = -1;
      return;
    }
    am_overlay_data_t *data = (am_overlay_data_t*)svc_ret->data;
    AM_STREAM_ID stream_id = AM_STREAM_ID(((am_overlay_id_t*)msg_data)
                                          ->stream_id);
    uint32_t area_id = ((am_overlay_id_t*)msg_data)->area_id;
    uint32_t index = ((am_overlay_id_t*)msg_data)->data_index;

    AMOverlayAreaData param;
    if (ol->get_area_data_param(stream_id, area_id, index, param)
        != AM_RESULT_OK) {
      svc_ret->ret = -1;
      return;
    }
    if (param.type == AM_OVERLAY_DATA_TYPE_NONE) {
      data->type = 404;
      break;
    }

    data->width = param.rect.size.width;
    data->height = param.rect.size.height;
    data->offset_x = param.rect.offset.x;
    data->offset_y = param.rect.offset.y;
    data->type = uint32_t(param.type);

    if ((AM_OVERLAY_DATA_TYPE_STRING == param.type) ||
        (AM_OVERLAY_DATA_TYPE_TIME == param.type)) {
      AMOverlayTextBox &text = param.text;
      data->spacing = text.spacing;
      AMOverlayCLUT c;
      c = text.background_color;
      data->bg_color = (c.v << 24) | (c.u << 16) | (c.y << 8) | c.a;
      uint32_t tmp = text.font_color.id;;
      if (tmp < 8) {
        data->font_color = tmp;
      } else {
        c = text.font_color.color;
        data->font_color = (c.v << 24) | (c.u << 16) | (c.y << 8) | c.a;
      }
      data->msec_en = text.en_msec;
      AMOverlayFont &font = text.font;
      data->font_size = font.width;
      data->font_outline_w = font.outline_width;
      c = text.outline_color;
      data->font_outline_color = (c.v << 24) | (c.u << 16) | (c.y << 8) | c.a;
      data->font_hor_bold = font.hor_bold;
      data->font_ver_bold = font.ver_bold;
      data->font_italic = font.italic;
    } else if ((AM_OVERLAY_DATA_TYPE_PICTURE == param.type) ||
        (AM_OVERLAY_DATA_TYPE_ANIMATION == param.type)) {
      AMOverlayCLUT &c = param.pic.colorkey.color;
      data->color_key = (c.v << 24) | (c.u << 16) | (c.y << 8) | c.a;
      data->color_range = param.pic.colorkey.range;
      data->bmp_num = param.pic.num;
      data->interval = param.pic.interval;
    }
  } while(0);

  return;
}

void ON_VIDEO_EIS_SET(void *msg_data,
                      int msg_data_size,
                      void *result_addr,
                      int result_max_size)
{
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  INFO("video service ON_VIDEO_EIS_SET!\n");

  do {
    am_encode_eis_ctrl_t *eis_config = nullptr;
    AMIEncodeEIS *eis =
        (AMIEncodeEIS*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_EIS);
    int32_t eis_mode = 0;
    svc_ret->ret = -1;
    if (AM_UNLIKELY(!msg_data)) {
      ERROR("NULL pointer of msg_data!\n");
      break;
    }

    if (AM_UNLIKELY(!eis)) {
      WARN("Video Plugin %s is not loaded!", VIDEO_PLUGIN_EIS_SO);
      break;
    }

    eis_config = (am_encode_eis_ctrl_t*)msg_data;
    if (TEST_BIT(eis_config->enable_bits, AM_ENCODE_EIS_MODE_EN_BIT)) {
      if (AM_RESULT_OK != eis->get_eis_mode(eis_mode)) {
        ERROR("Get EIS mode error");
        break;
      }
      if (AM_RESULT_OK != eis->set_eis_mode(eis_config->eis_mode)) {
        ERROR("Set EIS mode error");
        break;
      }
    }
    if (AM_RESULT_OK != eis->apply()) {
      ERROR("Failed to apply eis parameters! Reset to last one!");
      if (TEST_BIT(eis_config->enable_bits, AM_ENCODE_EIS_MODE_EN_BIT)) {
        if (AM_RESULT_OK != eis->set_eis_mode(eis_mode)) {
          ERROR("Reset EIS mode error");
          break;
        }
      }
      break;
    }
    svc_ret->ret = 0;

  } while(0);
}

void ON_VIDEO_EIS_GET(void *msg_data,
                      int msg_data_size,
                      void *result_addr,
                      int result_max_size)
{
  INFO("video service ON_EIS_GET");
  am_service_result_t *svc_ret = (am_service_result_t*)result_addr;
  memset(svc_ret, 0, sizeof(am_service_result_t));
  am_encode_eis_ctrl_t *eis_config = (am_encode_eis_ctrl_t*)svc_ret->data;

  do {
    AMIEncodeEIS *eis =
        (AMIEncodeEIS*)g_video_camera->get_video_plugin(VIDEO_PLUGIN_EIS);

    svc_ret->ret = -1;
    if (AM_UNLIKELY(!eis_config)) {
      ERROR("NULL pointer!\n");
      break;
    }

    if (AM_UNLIKELY(!eis)) {
      WARN("Video Plugin %s is not loaded!", VIDEO_PLUGIN_EIS_SO);
      break;
    }

    if (AM_RESULT_OK != eis->get_eis_mode(eis_config->eis_mode)) {
      ERROR("Failed to get EIS mode!");
      break;
    }

    svc_ret->ret = 0;
  } while (0);

}
